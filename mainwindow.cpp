#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "parser.h"
#include <QFileDialog>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->update->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_open_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Выбрать файл JSON"), "", tr("JSON(*.json *.txt)"));
    if(!path.isEmpty())
    {
        ui->path->setText(path);
        on_update_clicked();
    }
}

void MainWindow::on_update_clicked()
{
    ui->update->show();
    Parser parser(ui->path->text());
#ifdef EASY_WAY
    ui->textBrowser->setTextColor(parser.EASY_WAY()? Qt::darkGreen : Qt::red);
#else
    ui->textBrowser->setTextColor(parser.parse()? Qt::darkGreen : Qt::red);
#endif
    ui->textBrowser->setText(parser.About());
}
