#ifndef PARSER_H
#define PARSER_H

#include <QString>
#include <QFile>
#include <QStack>
#include <QDebug>
#include <QJsonParseError>
#include <QJsonDocument>
class Parser
{
public:
    // В конструкторе проще всего сохранить данные из файла в строку. На практике лучше так не делать, разумеется
    Parser(QString path)
    {
       QFile f(path);
       f.open(QIODevice::ReadOnly);
       data = QString::fromLocal8Bit(f.readAll());
    }

    // все что тут есть можно сделать в этой функции родными средствами Qt
    bool EASY_WAY(){
        QJsonParseError e;
        QJsonDocument doc = QJsonDocument::fromJson(data.toLocal8Bit(),&e);
        if(e.error != QJsonParseError::NoError)
        {
            errorCharacter = e.offset;
            return false;
        }
        return true;
    }


    // провести парсинг и вернуть успешность операции
    bool parse()
    {        
        /* идея в том, что бы идти по массиву с символов собирая знаки открытия блоков и удаляя их при нахождении их закрывающих
        * для этого идеально подходит организация стэка, так как кладем открывающий только вверх,
        * но на каждый открывающий будет встречен свой закрывающий в обратном порядке ( например: {"":""} или {"":[]} ) */

        QByteArray chars = data.toLocal8Bit();
        for(int i = 0; i < chars.size(); i++)
        {
            char ch = chars.at(i);
            if(!isExpected(ch))
            {
                errorCharacter = i;
                return false;
            }
        }
        return true;
    }


    // закрывающий символ?
    bool isClosing(char ch){
        switch (ch) {
        case '\"': return blocks.top() == '\"';
        case ']':
        case '}':
            return true;
        default:
            return false;
        }
    }

    // открывающий символ?
    bool isOpening(char ch)
    {
        switch (ch) {
        case '\"': return blocks.top() != '\"';
        case '{':
        case '[':
            return true;
        default:
            return false;
        }
    }

    // спец. символ?
    bool isSpecial(char ch){
        switch (ch) {
        case '{':
        case '[':
        case '\"':
        case '}':
        case ':':
        case ']':
        case ',':
            return true;
        default:
            return false;
        }
    }

    // пунктуационный символ?
    bool isPunc(char ch)
    {
        switch (ch) {
        case ',':
        case ':':
            return true;
        default:
            return false;
        }
    }

    // ожидался ли такой символ?
    bool isExpected(char ch){
        static char prevChar = '{';

        bool success = false;
        // если особый символ - то проверяем
        if(isSpecial(ch))
        {
            // если символ закрывающий, а в массиве блоков нет блоков для закрытия - ошибка
            if(isClosing(ch) && blocks.size() == 0)
                return false;

            // рас тут мы точно знаем что это спец символ - то можно проверять условия
            switch (ch) {
            case '{': success = blocks.size() == 0 || prevChar == ':' || prevChar == '[' || prevChar == ',';                                                break;
            case '[': success = prevChar == ':' || prevChar == '{';                                                                                         break;
            case '\"':success = (isOpening(ch) && (isPunc(prevChar) ||  prevChar == '\n' || prevChar == '{' || prevChar == '[')) || isClosing(ch);          break;
            case '}': success = blocks.top() == '{';                                                                                                        break;
            case ':': success = blocks.top() != '\"' && prevChar == '\"';                                                                                   break;
            case ']': success = blocks.top() == '[';                                                                                                        break;
            case ',': success = prevChar != '[' && !isPunc(prevChar) && (!isSpecial(prevChar) || prevChar == '\"' || prevChar == ']' || prevChar == '}');   break;
            default:
                return false;
            }

            // успешно ли прошли условия на соответствие
            if(success)
            {
                // закрывающий - удаляем верхний блок
                if(isClosing(ch))
                    blocks.pop();
                // открывающий - добавляем блок
                else if (isOpening(ch))
                    blocks.push(ch);
            }
        }
        // иначе обычный символ - точно ожидается
        else
            success = true;

        // запоминаем символ для того что бы смотреть на предыдущий в след раз, если это не пробел или каретка
        if(ch != '\n' && ch != ' ' && ch != '\r' && ch != '\t')
            prevChar = ch;

        return success;
    }

    // сформировать вывод
    QString About(){
        QString res;
        QByteArray json = data.toLocal8Bit();

        /* если есть ошибка, то нужно посчитать в какой строке какой именно символ вызвал ошибку.
        * для этого проходим весь массив данных, считая строки и колонки,
        * когда натыкаемся на искомый ошибочный символ - формируем сообщение об ош. и выходим с цикла */

        if(errorCharacter > -1) {
            int row=1,col=0;
            for(int i = 0; i < json.size(); i++)
            {
                if(json.at(i) == '\n')
                {
                    row++;
                    col = 0;
                }
                else
                    col++;
                if(i == errorCharacter)
                {
                    res = QString("Ошибка парсинга: %1:%2").arg(row).arg(col);
                    break;
                }
            }
        }

        // идея такая - идем по массиву с данными и добавляем каждую новую строчку нумерацию строки.
        QByteArray resultText;
        int row = 0;
        resultText.append(QString("%1|").arg(++row));
        for(int i = 0; i < json.size(); i++)
        {
            QString toappend;
            if(json.at(i) == '\n')
                toappend = QString("\n%1|").arg(++row);

            else
                toappend = json.at(i);

            resultText.append(toappend);
        }

        return QString("%1\n%2").arg(QString::fromLocal8Bit(resultText)).arg(res);
    }

    int errorCharacter = -1;
    QStack<char> blocks;
    QString data;
};

#endif // PARSER_H
